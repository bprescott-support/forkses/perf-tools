#!/usr/bin/env bash

set -euo pipefail
IFS=$'\n\t'

export DEBIAN_FRONTEND=noninteractive


function install_recent() {
  apt-get -yq update
  apt-get -yq install bpfcc-tools "linux-headers-$(uname -r)"
}

function install_older() {
  # Installs bcc and perf tools
  apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 4052245BD4284CDD
  echo "deb https://repo.iovisor.org/apt/$(lsb_release -cs) $(lsb_release -cs) main" | tee /etc/apt/sources.list.d/iovisor.list

  apt-get -yq update
  apt-get -yq install linux-tools-common linux-tools-generic "linux-tools-$(uname -r)" bcc-tools libbcc-examples "linux-headers-$(uname -r)"
}

# Install for more recent of ubunut, or older versions
install_recent || install_older
