#!/usr/bin/env bash

set -euo pipefail
IFS=$'\n\t'

PERF_TOOLS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." && pwd )"

pushd .
TEMP_DIR=$(mktemp -d --suffix=-perftools)
cd "$TEMP_DIR"

function finish {
  popd
  rm -rf "$TEMP_DIR"
}
trap finish EXIT

SVG_OUTPUT=$(mktemp --suffix=-perftools-system-cpu-flamegraph.svg)

echo "# Recording 60 second on-cpu flamegraph"
perf record -F 99 -a -g -- sleep 60
perf script | "$PERF_TOOLS_DIR/FlameGraph/stackcollapse-perf.pl" > out.perf-folded
"$PERF_TOOLS_DIR/FlameGraph/flamegraph.pl" out.perf-folded > "${SVG_OUTPUT}"

gzip "${SVG_OUTPUT}"
chmod 644 "${SVG_OUTPUT}.gz"

echo "# SVG saved to ${SVG_OUTPUT}.gz"
echo "# Retrieve it with: scp $(hostname -f):${SVG_OUTPUT}.gz . && gunzip $(basename "${SVG_OUTPUT}.gz") && open $(basename "${SVG_OUTPUT}")"
